---
layout: markdown_page
title: Product categories
---

## Introduction

Below is the canonical list of product categories, grouped by DevOps lifecycle
stage and non-lifecycle groups, along with the name of the responsible product manager.

We want intuitive interfaces both within the company and with the wider community.
This makes it more efficient for everyone to contribute or to get a question answered.
Therefore following interfaces are based on the product categories defined on this page:

- [Product Vision](https://about.gitlab.com/direction/product-vision/)
- [Direction](https://about.gitlab.com/direction/#functional-areas)
- [SDLC](https://about.gitlab.com/sdlc/#stacks)
- [Product Features](https://about.gitlab.com/features/)
- [Documentation](https://docs.gitlab.com/ee/)
- Our deck, the slides that we use to describe the company
- Engineering groups
- Product groups
- Product marketing specializations

![DevOps lifecycle](handbook/sales/devops-loop.svg)

At GitLab the Dev and Ops split is different because our CI/CD functionality is one codebase that falls under Ops.

## Dev

- Product: [Job]
- Backend: n/a

1. Plan - [Victor]
  - Chat integration
  - Issue Tracking
  - Issue Board
  - Portfolio Management
  - [Service Desk]
1. Create - [Victor] and [James]
  - Code Review - [Victor]
  - Version Control - [James]
  - Web IDE - [James]
  - [Geo] - [James]
  - Wiki - [James]
  - Gitaly - [James]
1. Auth - [Jeremy]
  - Signup
  - User management & authentication (incl. LDAP)
  - Groups and [Subgroups]
  - Audit log
  - DevOps Score (previously Conversational Development Index / ConvDev Index)
  - Cycle Analytics
  - [Usage statistics](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html) (incl. version.gitlab.com)
  - Subscriptions (incl. license.gitlab.com and customers.gitlab.com)
1. Quality - [Jeremy]
  - [Internationalization](https://docs.gitlab.com/ee/development/i18n/)
  - [GitLab QA](https://gitlab.com/gitlab-org/gitlab-qa)
  - [GDK](https://gitlab.com/gitlab-org/gitlab-development-kit)
1. Gitter - n/a

## Ops

- Product: [Mark]
- Backend: n/a

1. Verify - [Fabio]
  - [Continuous Integration (CI)]
  - GitLab Runner
1. Package - [Fabio]
  - Container Registry
  - Binary Repository
1. Release - [Fabio]
  - [Continuous Delivery (CD)] / Release Automation
  - [Pages]
  - Review apps
1. Configure - [Fabio]
  - Application Control Panel
  - Infrastructure Configuration
  - Operations
  - Feature Flags
  - ChatOps
  - PaaS (formerly Auto DevOps)
1. Monitor - [Josh]
  - Metrics
  - Tracing
  - Production monitoring
  - Error Tracking
  - Logging
1. BizOps - [Josh]
1. Security Products - n/a
  - Static Application Security Testing (SAST)
  - Dynamic application security testing (DAST)
  - Dependency Scanning
  - Container Scanning
  - License Management
  - Runtime Application Self-Protection (RASP)
1. Distribution - [Josh]
  - Omnibus
  - Cloud Native Installation

## Composed categories

GitLab also does the things below that are composed of multiple categories.

1. Software Composition Analysis (SCA) = Dependency Scanning + License Management
1. Interactive Application Security Testing (IAST) = Dynamic application security testing (DAST) + Runtime Application Self-Protection (RASP)
1. Application Performance Management (APM) = Metrics + Tracing

[Jeremy]: /handbook/product#jeremy-watson
[Fabio]: /handbook/product#fabio-busatto
[Josh]: /handbook/product#joshua-lambert
[Mark]: /handbook/product#mark-pundsack
[James]: /handbook/product#james-ramsay
[Job]: /handbook/product#job-van-der-voort
[Victor]: /handbook/product#Victor-wu
[Pages]: /features/pages/
[Geo]: /features/gitlab-geo/
[Continuous Integration (CI)]: /features/gitlab-ci-cd/
[Continuous Delivery (CD)]: /features/gitlab-ci-cd/
[Subgroups]: /features/subgroups/
[Service Desk]: /features/service-desk/
